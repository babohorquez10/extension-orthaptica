﻿using UnityEngine;
using UnityEngine.EventSystems;

public class RotarCamara : MonoBehaviour
{
    private bool botonPresionado = false;
    private string nombreBotonPresionado = "";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        const float rotSpeed = 0.1f;
        
        if(botonPresionado)
        {
            if(nombreBotonPresionado.Contains("Derecha")) transform.RotateAround(Vector3.zero, new Vector3(0.0f, 1.0f, 0.0f), -rotSpeed);
            
            else transform.RotateAround(Vector3.zero, new Vector3(0.0f, 1.0f, 0.0f), rotSpeed);
        }

    }

    public void OnPointerDownDelegate(BaseEventData data)
    {
        var click = (PointerEventData) data;

        if (click.button == PointerEventData.InputButton.Left)
        {
            botonPresionado = true;
            nombreBotonPresionado = data.selectedObject.name;
        }
    }

    public void OnPointerUpDelegate(BaseEventData data)
    {
        var click = (PointerEventData) data;

        if (click.button == PointerEventData.InputButton.Left) botonPresionado = false;

    }
}
