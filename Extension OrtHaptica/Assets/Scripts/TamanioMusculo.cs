﻿using UnityEngine;

public class TamanioMusculo : MonoBehaviour
{
    private const float delgado = 0.9f;
    private const float robusto = 1.1f;

    private GameObject puntoReferencia;
    private int tamanio;

    public Vector3 posicionInicial;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void asignarTamanio(int pTamanio)
    {
        tamanio = pTamanio;
    }

    public void cambiarTamanio()
    {
        puntoReferencia = new GameObject();
        puntoReferencia.name = "PuntoReferencia";

        puntoReferencia.transform.position = posicionInicial;
        puntoReferencia.transform.parent = gameObject.transform;

        if (tamanio == 0) cambiarTamanio(delgado);
        else if (tamanio == 2) cambiarTamanio(robusto);


        GameObject.Destroy(puntoReferencia);
    }


    public void cambiarTamanio(float nuevoTamanio)
    {
        transform.localScale = new Vector3(nuevoTamanio, 1f, nuevoTamanio);

        Vector3 cambioPosicion = puntoReferencia.transform.position - posicionInicial;
        transform.position -= cambioPosicion;

    }


}
