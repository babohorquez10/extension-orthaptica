﻿using UnityEngine;

public class SeleccionarSegmento : MonoBehaviour
{
    private GameObject puntoRotacion;
    private Corte scriptPadre;
    private Vector3 centro;
    private bool upperHull;
    private float direccion;
    public Vector3 rotacionTotal = new Vector3(0, 0, 0);
    public Vector3 cambioPosicionTotal = new Vector3(0, 0, 0);
    

    // Start is called before the first frame update
    void Start()
    {
        scriptPadre = GameObject.Find("Plane").GetComponent<Corte>();
        centro = transform.GetComponent<Renderer>().bounds.center;
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void esUpperHull(bool b)
    {
        upperHull = b;

        if (upperHull) direccion = 1f;
        else direccion = -1f;
    }

    public void agregarPuntoRotacion(GameObject puntoRot)
    {
        puntoRotacion = puntoRot;
        rotacionTotal = puntoRot.transform.rotation.eulerAngles;
        
    }

    public GameObject darPuntoRotacion()
    {
        return puntoRotacion;
    }


    private void OnMouseDown()
    {
        scriptPadre.seleccionarSegmento(gameObject);
    }

    public void rotar(KeyCode arrow, bool ctrlPresionado)
    {
        Vector3 dir = Vector3.zero;

        if (puntoRotacion == null)
        {
            centro = transform.GetComponent<Renderer>().bounds.center;

            if (arrow == KeyCode.UpArrow)
            {
                dir = Vector3.left;
            }
            else if (arrow == KeyCode.DownArrow)
            {
                dir = Vector3.right;
            }
            else if (arrow == KeyCode.RightArrow)
            {
                if(!ctrlPresionado) dir = Vector3.forward;
                
                else dir = Vector3.up;
                
            }
            else if (arrow == KeyCode.LeftArrow)
            {
                if (!ctrlPresionado) dir = Vector3.back;
                
                else dir = Vector3.down;
            }
 
            transform.RotateAround(centro, dir, 0.1f);
            rotacionTotal -= dir * 0.1f;
        }

        else
        {
            if (arrow == KeyCode.UpArrow)
            {
                dir = puntoRotacion.transform.right;
            }
            else if (arrow == KeyCode.DownArrow)
            {
                dir = -puntoRotacion.transform.right;
            }
            else if (arrow == KeyCode.RightArrow)
            {
                if (!ctrlPresionado) dir = -puntoRotacion.transform.forward;
                
                else dir = puntoRotacion.transform.up;     
            }

            else if (arrow == KeyCode.LeftArrow)
            {
                if (!ctrlPresionado) dir = puntoRotacion.transform.forward;
                
                else dir = -puntoRotacion.transform.up;
            }

            transform.RotateAround(puntoRotacion.transform.position, dir, 0.01f);

            if(dir == -puntoRotacion.transform.up || dir == puntoRotacion.transform.up) rotacionTotal -= dir * 0.01f;
            
            else rotacionTotal -= dir * direccion * 0.01f;   
        }
        
    }

    public void mover(Vector3 axe)
    {
        if (puntoRotacion == null)
        {
            transform.localPosition += axe * 0.001f;
            cambioPosicionTotal -= axe * 0.001f;
        }
    }


}
