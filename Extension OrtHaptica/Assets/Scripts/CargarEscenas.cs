﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CargarEscenas : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void cargarEscena(string nombre)
    {
        SceneManager.LoadScene(nombre);
    }

    public void cargarEscenaActual()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void salir()
    {
        Application.Quit();
    }
}
