﻿using UnityEngine;

public class SeleccionarHueso : MonoBehaviour
{
    private Corte scriptPadre;


    // Start is called before the first frame update
    void Start()
    {
        scriptPadre = GameObject.Find("Plane").GetComponent<Corte>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        scriptPadre.seleccionarHueso(gameObject);
    }
}
