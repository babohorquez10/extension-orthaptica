﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Guardar : MonoBehaviour
{
    public Material materialHueso;
    public float maxTriangulos = 256;
    private GameObject exportable;
    private ArrayList segmentos;
    private ArrayList segmentosArticulaciones;

    private GameObject tejidoUnificado;
    private GameObject tejidoSimplificado;

    public InputField inputFieldNombreCaso;
    public InputField inputFieldTipoFractura;
    public InputField inputFieldDificultadCaso;

    private string nombreCaso;
    private string tipoDeFractura;
    private string dificultadReduccion;

    private Text estado;

    void Start()
    {
        segmentos = new ArrayList();
        segmentosArticulaciones = new ArrayList();

        estado = GameObject.Find("EstadoApp").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void guardar()
    {
        nombreCaso = inputFieldNombreCaso.text;
        tipoDeFractura = inputFieldTipoFractura.text;
        dificultadReduccion = inputFieldDificultadCaso.text;

        guardarExportableSinSimplificar();
        
        GameObject dummy = GameObject.Instantiate(this.gameObject);

        foreach (Transform hijo in dummy.transform)
        {
            MeshRenderer mRend = hijo.GetComponentInChildren<MeshRenderer>();
            if (mRend != null)
            {
                GameObject segmento = mRend.gameObject;
                simplificarObjeto(segmento);
                foreach (GameObject obj in segmentos)
                {
                    if (obj.name.Equals(segmento.name))
                    {
                        
                        segmento.transform.parent = obj.transform.parent;

                        if (segmento.transform.parent.gameObject.name.Contains("Sup1")) segmento.name = "Sup1_simple";
                        else if (segmento.transform.parent.gameObject.name.Contains("Inf1")) segmento.name = "Inf1_simple";
                        else if (segmento.transform.parent.gameObject.name.Contains("Sup2")) segmento.name = "Sup2_simple";
                        else if (segmento.transform.parent.gameObject.name.Contains("Inf2")) segmento.name = "Inf2_simple";
                        else if (segmento.transform.parent.gameObject.name.Contains("Sup")) segmento.name = "Sup_simple";
                        else if (segmento.transform.parent.gameObject.name.Contains("Inf")) segmento.name = "Inf_simple";
                        else segmento.name = "Intermedio_simple";

                    }

                }
            }

        }

        Destroy(dummy);

        combinarMallaTejido();
        generarJSON();

        guardarCasoFracturaCompleto();

        estado.text = "¡Caso guardado con éxito!";
    }

    private void guardarExportableSinSimplificar()
    {

        int i = 0;

        foreach (Transform hijo in gameObject.transform)
        {

            if (hijo.name.Contains("Hull"))
            {
                hijo.gameObject.GetComponent<MeshRenderer>().material = materialHueso;
                hijo.name = "Segmento" + i;

                GameObject pRot = hijo.gameObject.GetComponent<SeleccionarSegmento>().darPuntoRotacion();

                if (pRot != null)
                {
                    segmentosArticulaciones.Add(hijo.gameObject);

                    if (pRot.name.Contains("Sup1")) hijo.name = "Sup1_touchable";
                    else if (pRot.name.Contains("Inf1")) hijo.name = "Inf1_touchable";
                    else if (pRot.name.Contains("Sup2")) hijo.name = "Sup2_touchable";
                    else if (pRot.name.Contains("Inf2")) hijo.name = "Inf2_touchable";
                    else if (pRot.name.Contains("Sup")) hijo.name = "Sup_touchable";
                    else hijo.name = "Inf_touchable";
                    
                }
                else
                {
                    hijo.name = "Intermedio_touchable_" + "Segmento" + i;
                }

                segmentos.Add(hijo.gameObject);
                i++;


            }

        }

        gameObject.name = nombreCaso;

        asignarArticulaciones();
        crearCentrosDeMasa();
        crearVectores();
    }

    private void combinarMallaTejido()
    {
        GameObject tejido = gameObject.transform.Find("Tejido").gameObject;
        GameObject actual = null;

        Material materialMusculo = tejido.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material;

        foreach (Transform child in tejido.transform)
        {
            actual = child.gameObject;

            if (!actual.activeSelf) Destroy(actual);
        }

    }

    private void simplificarTejido()
    {
        tejidoSimplificado = GameObject.Instantiate(tejidoUnificado);
        tejidoSimplificado.transform.parent = gameObject.transform;
        simplificarObjeto(tejidoSimplificado);

        tejidoSimplificado.SetActive(false);
    }

    private void crearVectores()
    {
        foreach (GameObject segmento in segmentos)
        {
            string pName = "";

            if (segmento.GetComponent<SeleccionarSegmento>().darPuntoRotacion() == null)
            {
                pName = "Intermedio_vector";
            }
            else
            {
                pName = segmento.name.Replace("touchable", "vector");
            }

            GameObject vector = new GameObject
            {
                name = pName
            };

            vector.transform.position = segmento.GetComponent<Renderer>().bounds.center;
            vector.transform.parent = segmento.transform.parent;
        }

    }

    private void asignarArticulaciones()
    {
        foreach (GameObject segmento in segmentosArticulaciones)
        {
            GameObject puntoRot = segmento.GetComponent<SeleccionarSegmento>().darPuntoRotacion();

            puntoRot.name = puntoRot.name.Replace("PuntoRotacion", "") + "_Segmento_PuntoArticulacion";

            segmento.transform.parent = puntoRot.transform;

        }
    }

    private void crearCentrosDeMasa()
    {
        foreach (GameObject segmento in segmentos)
        {
            GameObject puntoRot = segmento.GetComponent<SeleccionarSegmento>().darPuntoRotacion();
            string pNombre = "Intermedio_" + segmento.name;

            if (puntoRot != null)
            {
                if (puntoRot.name.Contains("Sup1")) pNombre = "Sup1_collider";
                else if (puntoRot.name.Contains("Inf1")) pNombre = "Inf1_collider";
                else if (puntoRot.name.Contains("Sup2")) pNombre = "Sup2_collider";
                else if (puntoRot.name.Contains("Inf2")) pNombre = "Inf2_collider";
                else if (puntoRot.name.Contains("Sup")) pNombre = "Sup_collider";
                else pNombre = "Inf_collider";

            }
            else
            {
                pNombre = segmento.name.Replace("touchable_", "");
            }

            GameObject centroMasa = new GameObject
            {
                name = pNombre
            };

            centroMasa.transform.position = segmento.GetComponent<Renderer>().bounds.center;
            centroMasa.transform.parent = segmento.transform.parent;

            segmento.transform.parent = centroMasa.transform;

            if (puntoRot != null) centroMasa.transform.parent = puntoRot.transform;

        }

    }

    private void simplificarObjeto(GameObject obj)
    {
        MeshFilter mFilter = obj.GetComponent<MeshFilter>();

        if (mFilter != null)
        {
            Mesh sourceMesh = mFilter.mesh;
            float numTirangulos = sourceMesh.triangles.Length;
            int iter = 0;

            while (numTirangulos > maxTriangulos && iter < 3)
            {
                float quality = maxTriangulos / numTirangulos;

                var meshSimplifier = new UnityMeshSimplifier.MeshSimplifier();
                meshSimplifier.Initialize(sourceMesh);
                meshSimplifier.SimplifyMesh(quality);

                var destMesh = meshSimplifier.ToMesh();

                obj.GetComponent<MeshFilter>().mesh = destMesh;

                numTirangulos = destMesh.triangles.Length;
                sourceMesh = destMesh;
                iter++;
            }

        }

    }

    private void generarJSON()
    {
        Archivo arch = new Archivo
        {
            nombre = nombreCaso,
            numSegmentos = segmentos.Count,
            tipoFractura = tipoDeFractura,
            dificultades = dificultadReduccion,
            segmentos = new Segmento[segmentos.Count]
        };

        for (int i = 0; i < segmentos.Count; i++)
        {
            GameObject actual = (GameObject) segmentos[i];
            SeleccionarSegmento scrSegmento = actual.GetComponent<SeleccionarSegmento>();
            Vector3 rotaciones = scrSegmento.rotacionTotal;
            Vector3 pos = scrSegmento.cambioPosicionTotal;

            arch.segmentos[i] = new Segmento
            {
                nombreSegmento = actual.name,
                rotacionOriginal = new float[3],
                posicionOriginal = new float[3]
            };

            arch.segmentos[i].rotacionOriginal[0] = rotaciones.x;
            arch.segmentos[i].rotacionOriginal[1] = rotaciones.y;
            arch.segmentos[i].rotacionOriginal[2] = rotaciones.z;

            if (scrSegmento.darPuntoRotacion() != null)
            {
                arch.segmentos[i].posicionOriginal[0] = actual.transform.parent.parent.transform.localPosition.x + pos.x;
                arch.segmentos[i].posicionOriginal[1] = actual.transform.parent.parent.transform.localPosition.y + pos.y;
                arch.segmentos[i].posicionOriginal[2] = actual.transform.parent.parent.transform.localPosition.z + pos.z;
            }
            else
            {
                arch.segmentos[i].posicionOriginal[0] = actual.transform.parent.transform.localPosition.x + pos.x;
                arch.segmentos[i].posicionOriginal[1] = actual.transform.parent.transform.localPosition.y + pos.y;
                arch.segmentos[i].posicionOriginal[2] = actual.transform.parent.transform.localPosition.z + pos.z;
            }

            GameObject objeto = actual.transform.parent.parent.gameObject;

            if (scrSegmento.darPuntoRotacion() == null) objeto = actual.transform.parent.gameObject;

            GameObject copiaGuia = GameObject.Instantiate(objeto);
            copiaGuia.transform.parent = objeto.transform.parent;

            copiaGuia.transform.localScale = objeto.transform.localScale;
            copiaGuia.transform.localPosition =
                new Vector3(arch.segmentos[i].posicionOriginal[0],
                arch.segmentos[i].posicionOriginal[1],
                arch.segmentos[i].posicionOriginal[2]);

            copiaGuia.transform.rotation = Quaternion.Euler(rotaciones);

            if (scrSegmento.darPuntoRotacion() != null)
            {
                if (scrSegmento.darPuntoRotacion().name.Contains("Sup1")) simplificarGuia(copiaGuia, "Sup1");
                else if (scrSegmento.darPuntoRotacion().name.Contains("Inf1")) simplificarGuia(copiaGuia, "Inf1");
                else if (scrSegmento.darPuntoRotacion().name.Contains("Sup2")) simplificarGuia(copiaGuia, "Sup2");
                else if (scrSegmento.darPuntoRotacion().name.Contains("Inf2")) simplificarGuia(copiaGuia, "Inf2");
                else if (scrSegmento.darPuntoRotacion().name.Contains("Sup")) simplificarGuia(copiaGuia, "Sup");
                else simplificarGuia(copiaGuia, "Inf");

            }
            else
            {
                GameObject simple = copiaGuia.transform.Find("Intermedio_simple").gameObject;
                GameObject vector = copiaGuia.transform.Find("Intermedio_vector").gameObject;

                simple.SetActive(false);
                vector.SetActive(false);

                Destroy(simple);
                Destroy(vector);

                copiaGuia.transform.GetChild(0).name = "Guia_hijo";
            }

            copiaGuia.name = "Guia_" + objeto.name.Replace("_Segmento_PuntoArticulacion", "");
        }

        string json = JsonUtility.ToJson(arch);

        string filePath = "./Casos/" + nombreCaso + ".json";
        File.WriteAllText(filePath, json);

    }

    private void simplificarGuia(GameObject copiaGuia, string pNombre)
    {
        GameObject simple = copiaGuia.transform.Find(pNombre + "_collider").Find(pNombre + "_simple").gameObject;
        GameObject touchable = copiaGuia.transform.Find(pNombre + "_collider").Find(pNombre + "_vector").gameObject;

        simple.SetActive(false);
        touchable.SetActive(false);

        Destroy(simple);
        Destroy(touchable);

        GameObject temp = copiaGuia.transform.Find(pNombre + "_collider").Find(pNombre + "_touchable").gameObject;

        temp.name = "Guia_hijo";
    }

    private void guardarCasoFracturaCompleto()
    {
        ObjetoSerializable completo = recursion(gameObject);

        string json = JsonUtility.ToJson(completo);

        string filePath = "./Casos/" + nombreCaso + "_Exportado.json";
        File.WriteAllText(filePath, json);
    }

    private ObjetoSerializable recursion(GameObject objeto)
    {
        MeshFilter mFilt = objeto.GetComponent<MeshFilter>();

        ObjetoSerializable resp = new ObjetoSerializable
        {
            nombre = objeto.name,
            tieneMeshFilter = mFilt != null
        };

        Transform objTransform = objeto.transform;

        resp.posicion = objTransform.localPosition;
        resp.escala = objTransform.localScale;

        resp.rotW = objTransform.localRotation.w;
        resp.rotX = objTransform.localRotation.x;
        resp.rotY = objTransform.localRotation.y;
        resp.rotZ = objTransform.localRotation.z;

        if(resp.tieneMeshFilter)
        {
            Mesh malla = mFilt.mesh;

            resp.triangles = malla.triangles;
            resp.uv = malla.uv;
            resp.vertices = malla.vertices;
        }

        if(objTransform.childCount != 0)
        {
            ArrayList activos = new ArrayList();

            for(int i = 0; i < objTransform.childCount; i++)
            {
                GameObject actual = objTransform.GetChild(i).gameObject;

                if (actual.activeSelf) activos.Add(recursion(actual));
                
            }

            resp.objetosHijos = new ObjetoSerializable[activos.Count];

            for (int i = 0; i < activos.Count; i++)
            {
                resp.objetosHijos[i] = (ObjetoSerializable) activos[i];
            }
        }

        return resp;
    }

    [Serializable]
    public class ObjetoSerializable
    {
        public string nombre;
        public bool tieneMeshFilter;
        public ObjetoSerializable[] objetosHijos;
        public Vector3[] vertices;
        public Vector2[] uv;
        public int[] triangles;
        public float rotW;
        public float rotX;
        public float rotY;
        public float rotZ;
        public Vector3 posicion;
        public Vector3 escala;

    }

    [Serializable]
    public class Archivo
    {
        public string nombre;
        public int numSegmentos;
        public string tipoFractura;
        public string dificultades;
        public Segmento[] segmentos;

    }

    [Serializable]
    public class Segmento
    {
        public string nombreSegmento;
        public float[] rotacionOriginal;
        public float[] posicionOriginal;
    }


}
