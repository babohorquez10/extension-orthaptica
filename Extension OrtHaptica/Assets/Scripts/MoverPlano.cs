﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MoverPlano : MonoBehaviour
{
    private float speed = 0.0001f;
    private float rotSpeed = 0.05f;

    private bool botonPresionado = false;
    private string nombreBotonPresionado = "";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnPointerDownDelegate(BaseEventData data)
    {
        var click = (PointerEventData) data;

        if (click.button == PointerEventData.InputButton.Left)
        {
            botonPresionado = true;
            nombreBotonPresionado = data.selectedObject.name;
        }
    }

    public void OnPointerUpDelegate(BaseEventData data)
    {
        var click = (PointerEventData) data;

        if (click.button == PointerEventData.InputButton.Left) botonPresionado = false;
        
    }

    private void Update()
    {

        if(botonPresionado)
        {
            switch (nombreBotonPresionado)
            {
                case "Arriba":

                    transform.position += Vector3.up * speed;
                    break;

                case "RotarArriba":

                    transform.Rotate(-rotSpeed, 0, 0, Space.Self);
                    break;

                case "Abajo":

                    transform.position += Vector3.up * -speed;
                    break;

                case "RotarAbajo":

                    transform.Rotate(rotSpeed, 0, 0, Space.Self);
                    break;

                case "Derecha":

                    transform.position += Vector3.right * -speed;
                    break;

                case "RotarDerecha":

                    transform.Rotate(0, 0, rotSpeed, Space.Self);
                    break;

                case "Izquierda":

                    transform.position += Vector3.right * speed;
                    break;

                case "RotarIzquierda":

                    transform.Rotate(0, 0, -rotSpeed, Space.Self);
                    break;

            }
        }

    }

}


