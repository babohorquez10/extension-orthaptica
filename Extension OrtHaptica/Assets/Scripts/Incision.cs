﻿using System.Collections;
using UnityEngine;
using EzySlice;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Incision : MonoBehaviour
{
    private GameObject supIzq;
    private GameObject supDer;
    private GameObject infDer;
    private GameObject infIzq;
    private GameObject der;
    private GameObject izq;

    private ArrayList musculos;

    private bool hacerTransparente = false;
    private bool corteRealizado = false;

    public Material materialOriginal;
    public Material materialTransparente;
    public GameObject tejido;

    private const float speed = 0.0001f;
    private const float rotSpeed = 0.05f;

    private bool botonPresionado = false;
    private string nombreBotonPresionado = "";

    private Text estado;

    // Start is called before the first frame update
    void Start()
    {
        supIzq = transform.Find("SupIzq").gameObject;
        supDer = transform.Find("SupDer").gameObject;
        infDer = transform.Find("InfDer").gameObject;
        infIzq = transform.Find("InfIzq").gameObject;
        der = transform.Find("Der").gameObject;
        izq = transform.Find("Izq").gameObject;

        estado = GameObject.Find("EstadoApp").GetComponent<Text>();
    }

    public void OnPointerDownDelegate(BaseEventData data)
    {
        var click = (PointerEventData)data;

        if (click.button == PointerEventData.InputButton.Left)
        {
            botonPresionado = true;
            nombreBotonPresionado = data.selectedObject.name;
        }
    }

    public void OnPointerUpDelegate(BaseEventData data)
    {
        var click = (PointerEventData)data;

        if (click.button == PointerEventData.InputButton.Left) botonPresionado = false;

    }

    // Update is called once per frame
    void Update()
    {

        if (botonPresionado)
        {
            switch (nombreBotonPresionado)
            {
                case "Arriba":

                    transform.position += transform.up * speed;
                    break;

                case "RotarArriba":

                    transform.RotateAround(transform.position, transform.forward, -rotSpeed);
                    break;

                case "Abajo":

                    transform.position -= transform.up * speed;
                    break;

                case "RotarAbajo":

                    transform.RotateAround(transform.position, transform.forward, rotSpeed);
                    break;

                case "Derecha":

                    transform.position -= transform.forward * speed;
                    break;

                case "RotarDerecha":

                    transform.RotateAround(transform.position, transform.up, -rotSpeed);
                    break;

                case "Izquierda":

                    transform.position += transform.forward * speed;
                    break;

                case "RotarIzquierda":

                    transform.RotateAround(transform.position, transform.up, rotSpeed);
                    break;

                case "Adelante":

                    transform.position += transform.right * speed;
                    break;

                case "Atras":

                    transform.position -= transform.right * speed;
                    break;
            }
        }

    }

    public void tejidoTransparente()
    {
        if (!corteRealizado)
        {
            hacerTransparente = !hacerTransparente;
            cambiarEstadoTejido();
        }
    }

    void cambiarEstadoTejido()
    {
        Material temp;

        if (hacerTransparente) temp = materialTransparente;
        else temp = materialOriginal;

        foreach (Transform child in tejido.transform)
        {
            GameObject actual = child.gameObject;

            if (actual.activeSelf) actual.GetComponent<MeshRenderer>().material = temp;
            
        }
    }

    public void realizarCorte()
    {
        darIntersectados(supIzq);
        darIntersectados(supDer);
        darIntersectados(infDer);
        darIntersectados(infIzq);
        darIntersectados(izq);
        darIntersectados(der);

        ponerColliders();

        quitarCortados("ZonaCorteSup");
        quitarCortados("ZonaCorteMedio");
        quitarCortados("ZonaCorteInf");
        
        if (hacerTransparente)
        {
            hacerTransparente = false;
            cambiarEstadoTejido();
        }

        corteRealizado = true;
        gameObject.SetActive(false);

        tejido.GetComponent<TamanioMusculo>().cambiarTamanio();
        estado.text = "Incisión realizada";
    }

    void darIntersectados(GameObject plano)
    {
        Bounds bnds = plano.GetComponent<MeshRenderer>().bounds;

        actualizarMusculos();

        foreach (GameObject child in musculos)
        {
            if (bnds.Intersects(child.GetComponent<MeshRenderer>().bounds)) hacerIncision(child, plano);
        }

    }

    void actualizarMusculos()
    {
        musculos = new ArrayList();

        foreach (Transform child in tejido.transform)
        {
            GameObject actual = child.gameObject;

            if (actual.activeSelf) musculos.Add(actual);
            
        }
    }

    public void hacerIncision(GameObject objCortar, GameObject plano)
    {
        Material crossSectionMaterial = objCortar.GetComponent<MeshRenderer>().material;
        SlicedHull hull = objCortar.Slice(plano.transform.position, plano.transform.up, crossSectionMaterial);

        if (hull != null)
        {
            GameObject low = hull.CreateLowerHull(objCortar, crossSectionMaterial);
            GameObject up = hull.CreateUpperHull(objCortar, crossSectionMaterial);

            low.transform.position = objCortar.transform.position;
            low.transform.localRotation = objCortar.transform.rotation;
            low.transform.localScale = objCortar.transform.lossyScale;

            up.transform.position = objCortar.transform.position;
            up.transform.localRotation = objCortar.transform.rotation;
            up.transform.localScale = objCortar.transform.lossyScale;

            if (objCortar.transform.parent != null)
            {
                low.transform.parent = objCortar.transform.parent;
                up.transform.parent = objCortar.transform.parent;
            }

            objCortar.SetActive(false);
        }


    }

    void ponerColliders()
    {
        actualizarMusculos();

        foreach (GameObject child in musculos) child.AddComponent<MeshCollider>();
    }



    void quitarCortados(string nombreZona)
    {
        Collider[] colisiones = darColisiones(gameObject.transform.Find(nombreZona).gameObject);
        foreach (Collider actual in colisiones)
        {
            //Eliminar los pedazos de músculo que estén dentro de la zona de corte, pero NO eliminar los huesos que estén en esa zona.
            if(actual.gameObject.transform.parent.gameObject.name.Contains("Tejido")) actual.gameObject.SetActive(false);
        }

    }


    private Collider[] darColisiones(GameObject obj)
    {
        Collider[] hitColliders = Physics.OverlapBox(obj.transform.position, obj.transform.lossyScale / 2, obj.transform.rotation, ~0);
        return hitColliders;
    }
}
