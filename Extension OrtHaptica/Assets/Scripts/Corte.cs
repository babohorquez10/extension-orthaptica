﻿using UnityEngine;
using EzySlice;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class Corte : MonoBehaviour
{
    public GameObject objeto;
    public Material crossSectionMaterial;
    public Material materialCorte;
    public GameObject tejido;
    public Material materialSeleccionado;

    public GameObject controles;
    public Toggle toggleTejido;

    private GameObject segmentoSeleccionado = null;
    private GameObject incision;
    private GameObject puntoRotacionSup;
    private GameObject puntoRotacionInf;
    private SeleccionarSegmento scriptSegmento = null;

    private bool moverSegmentos = false;
    private GameObject backPlane;

    // Puede ser null si solo hay 1 hueso en el escenario.
    private GameObject puntoRotacionSup2;
    private GameObject puntoRotacionInf2;

    private bool esFracturaCompleja;

    private bool botonPresionado = false;
    private string nombreBotonPresionado = "";

    private ArrayList botonesMover;
    private Text estado;

    // Start is called before the first frame update
    void Start()
    {
        incision = GameObject.Find("ObjetoIncision");
        incision.SetActive(false);

        puntoRotacionSup = GameObject.Find("PuntoRotacionSup");
        puntoRotacionInf = GameObject.Find("PuntoRotacionInf");
        backPlane = transform.Find("Back").gameObject;

        if (objeto != null && crossSectionMaterial == null)
        {
            crossSectionMaterial = objeto.GetComponent<MeshRenderer>().material;
            segmentoSeleccionado = objeto;
        }

        string tagHueso = objeto.tag;
        esFracturaCompleja = tagHueso.Contains("Hueso");

        // Hay más de 1 hueso en el escenario, de lo contrario no habría ningún elemento con este tag.
        if(esFracturaCompleja)
        {
            objeto.AddComponent<MeshCollider>();
            objeto.AddComponent<SeleccionarHueso>();
            objeto.GetComponent<MeshRenderer>().material = materialSeleccionado;
            GameObject otroHueso = null;

            if(tagHueso.Equals("Hueso1"))
            {
                otroHueso = GameObject.FindGameObjectWithTag("Hueso2");
            }
            else
            {
                otroHueso = GameObject.FindGameObjectWithTag("Hueso1");
            }

            otroHueso.AddComponent<MeshCollider>();
            otroHueso.AddComponent<SeleccionarHueso>();

            puntoRotacionSup = GameObject.Find("PuntoRotacionSup1");
            puntoRotacionInf = GameObject.Find("PuntoRotacionInf1");
            puntoRotacionSup2 = GameObject.Find("PuntoRotacionSup2");
            puntoRotacionInf2 = GameObject.Find("PuntoRotacionInf2");
        }

        tejido.GetComponent<TamanioMusculo>().posicionInicial = objeto.transform.GetComponent<Renderer>().bounds.center;

        botonesMover = new ArrayList();
        estado = GameObject.Find("EstadoApp").GetComponent<Text>();
    }

    public void OnPointerDownDelegate(BaseEventData data)
    {
        var click = (PointerEventData) data;

        if (data.selectedObject != null && click.button == PointerEventData.InputButton.Left)
        {
            botonPresionado = true;
            nombreBotonPresionado = data.selectedObject.name;
        }
    }

    public void OnPointerUpDelegate(BaseEventData data)
    {
        var click = (PointerEventData) data;

        if (click.button == PointerEventData.InputButton.Left) botonPresionado = false;

    }

    // Update is called once per frame
    void Update()
    {
        if(moverSegmentos && botonPresionado)
        {
            switch (nombreBotonPresionado)
            {
                case "Arriba":

                    scriptSegmento.mover(Vector3.up);
                    break;

                case "RotarArriba":

                    scriptSegmento.rotar(KeyCode.UpArrow, false);
                    break;

                case "Abajo":

                    scriptSegmento.mover(Vector3.down);
                    break;

                case "RotarAbajo":

                    scriptSegmento.rotar(KeyCode.DownArrow, false);
                    break;

                case "Derecha":

                    scriptSegmento.mover(Vector3.left);
                    break;

                case "RotarDerecha":

                    scriptSegmento.rotar(KeyCode.RightArrow, false);
                    break;

                case "Izquierda":

                    scriptSegmento.mover(Vector3.right);
                    break;

                case "RotarIzquierda":

                    scriptSegmento.rotar(KeyCode.LeftArrow, false);
                    break;

                case "Adelante":

                    scriptSegmento.mover(Vector3.back);
                    break;

                case "RotarAdelante":

                    scriptSegmento.rotar(KeyCode.RightArrow, true);
                    break;

                case "Atras":

                    scriptSegmento.mover(Vector3.forward);
                    break;

                case "RotarAtras":

                    scriptSegmento.rotar(KeyCode.LeftArrow, true);
                    break;
            }
            
        }
    }

    public void activarMoverSegmentos()
    {
        moverSegmentos = true;

        //Quitar plano de corte ya que los cortes ya se realizaron.
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        backPlane.GetComponent<MeshRenderer>().enabled = false;

        GameObject transformPadre = GameObject.Instantiate(tejido.transform.parent.gameObject);
        transformPadre.SetActive(false);

        foreach(Transform hijo in controles.transform)
        {
            Button objActual = hijo.gameObject.GetComponent<Button>();

            if(!objActual.name.Contains("Rotar"))
            {
                botonesMover.Add(objActual);
            }
        }

        scriptSegmento = segmentoSeleccionado.GetComponent<SeleccionarSegmento>();
        
        if (scriptSegmento.darPuntoRotacion() != null)
        {
            foreach (Button obj in botonesMover)
            {
                obj.interactable = false;
            }

        }
        else
        {
            foreach (Button obj in botonesMover)
            {
                obj.interactable = true;
            }
        }

        estado.text = "Moviendo segmentos";
    }

    public void realizarCorte()
    {
        bool corteValido = SliceObject(segmentoSeleccionado);

        if(corteValido)
        {
            GameObject inf = Object.Instantiate(backPlane, backPlane.transform.position, backPlane.transform.rotation);
            inf.transform.localScale = backPlane.GetComponentInParent<Transform>().lossyScale;

            GameObject sup = Object.Instantiate(inf);
            sup.transform.Rotate(0f, 0f, 180, Space.Self);

            sup.GetComponent<MeshRenderer>().material = materialCorte;
            inf.GetComponent<MeshRenderer>().material = materialCorte;

            sup.tag = "PlanoCorte";
            inf.tag = "PlanoCorte";
        }

    }

    public void tipoTejido(Dropdown drop)
    {
        activarTejido(drop.value);
        estado.text = "Ubicando incisión";
    }

    public void activarTejido(int tipoTejido)
    {
        tejido.SetActive(true);
        tejido.GetComponent<TamanioMusculo>().asignarTamanio(tipoTejido);
        
        Transform transformPadre = tejido.transform.parent.transform;

        GameObject actual = null;

        foreach (Transform child in transformPadre)
        {
            actual = child.gameObject;

            if (actual.activeSelf && (actual.name == "Lower_Hull" || actual.name == "Upper_Hull"))
            {
                //actual.GetComponent<MeshCollider>().enabled = false;
                actual.GetComponent<MeshRenderer>().material = materialSeleccionado;
            }
        }

        GameObject[] arr = GameObject.FindGameObjectsWithTag("PlanoCorte");

        foreach (GameObject actualObj in arr) actualObj.SetActive(false);
        
        incision.SetActive(true);
        Incision scr = incision.GetComponent<Incision>();
        scr.tejido = tejido;

        moverSegmentos = false;

        toggleTejido.gameObject.SetActive(true);
        scr.tejidoTransparente();
    }

    public void seleccionarSegmento(GameObject segmento)
    {
        segmentoSeleccionado.GetComponent<MeshRenderer>().material = crossSectionMaterial;

        segmentoSeleccionado = segmento;
        scriptSegmento = segmentoSeleccionado.GetComponent<SeleccionarSegmento>();

        segmentoSeleccionado.GetComponent<MeshRenderer>().material = materialSeleccionado;

        if(scriptSegmento.darPuntoRotacion() != null)
        {
            foreach (Button obj in botonesMover)
            {
                obj.interactable = false;
            }

        }
        else
        {
            foreach (Button obj in botonesMover)
            {
                obj.interactable = true;
            }
        }

    }

    public void seleccionarHueso(GameObject hueso)
    {
        segmentoSeleccionado.GetComponent<MeshRenderer>().material = crossSectionMaterial;
        segmentoSeleccionado = hueso;
        segmentoSeleccionado.GetComponent<MeshRenderer>().material = materialSeleccionado;
    }

    public bool SliceObject(GameObject objCortar)
    {

        SlicedHull hull = objCortar.Slice(transform.position, transform.up, crossSectionMaterial);

        if (hull != null)
        {
            GameObject low = hull.CreateLowerHull(objCortar, crossSectionMaterial);
            GameObject up = hull.CreateUpperHull(objCortar, crossSectionMaterial);

            low.transform.position = objCortar.transform.position;
            low.transform.localRotation = objCortar.transform.rotation;
            low.transform.localScale = objCortar.transform.lossyScale;

            up.transform.position = objCortar.transform.position;
            up.transform.localRotation = objCortar.transform.rotation;
            up.transform.localScale = objCortar.transform.lossyScale;
            

            if(objCortar.transform.parent != null)
            {
                low.transform.parent = objCortar.transform.parent;
                up.transform.parent = objCortar.transform.parent;

            }

            agregarScriptSeleccion(up, low, objCortar);
            seleccionarSegmento(up);
            low.GetComponent<MeshRenderer>().material = crossSectionMaterial;

            return true;
        }

        return false;
    }


    private void agregarScriptSeleccion(GameObject hullUp, GameObject hullLow, GameObject objOriginal)
    {
        hullUp.AddComponent<MeshCollider>();
        SeleccionarSegmento scrUp = hullUp.AddComponent<SeleccionarSegmento>();

        hullLow.AddComponent<MeshCollider>();
        SeleccionarSegmento scrLow = hullLow.AddComponent<SeleccionarSegmento>();

        SeleccionarSegmento scrPadre = objOriginal.GetComponent<SeleccionarSegmento>();

        scrUp.esUpperHull(true);
        scrLow.esUpperHull(false);

        string tagObjeto = objOriginal.tag;

        if(scrPadre == null)
        {

            // Hay más de 1 hueso en la escena, entonces hay más de 1 punto de rotación superior y más de uno inferior.
            if (esFracturaCompleja)
            {
                if(tagObjeto.Equals("Hueso1"))
                {
                    scrUp.agregarPuntoRotacion(puntoRotacionSup);
                    scrLow.agregarPuntoRotacion(puntoRotacionInf);
                }
                else
                {
                    scrUp.agregarPuntoRotacion(puntoRotacionSup2);
                    scrLow.agregarPuntoRotacion(puntoRotacionInf2);
                }
            }
            // Solo hay 1 hueso.
            else
            {
                scrUp.agregarPuntoRotacion(puntoRotacionSup);
                scrLow.agregarPuntoRotacion(puntoRotacionInf);
            }

        }
        else
        {
            GameObject puntoRotPadre = scrPadre.darPuntoRotacion();

            if (puntoRotPadre == puntoRotacionSup) scrUp.agregarPuntoRotacion(puntoRotacionSup);

            else if (puntoRotPadre == puntoRotacionInf) scrLow.agregarPuntoRotacion(puntoRotacionInf);

            // Hay más de 1 hueso en la escena, entonces hay más de 1 punto de rotación superior y más de uno inferior.
            if (esFracturaCompleja)
            {
                if (puntoRotPadre == puntoRotacionSup2) scrUp.agregarPuntoRotacion(puntoRotacionSup2);

                else if (puntoRotPadre == puntoRotacionInf2) scrLow.agregarPuntoRotacion(puntoRotacionInf2);
            }
            
        }

        GameObject.Destroy(objOriginal);
    }

}
