# Extension OrtHaptica

### Ambiente interactivo para creación de casos de fractura para entrenamiento en reducción en cirugía ortopédica.

Sistema interactivo que permite a docentes expertos en cirugía ortopédica definir casos de fractura en huesos
longitudinales a los cuales los residentes en entrenamiento se deben enfrentar. Permite definir la ruptura de los huesos
involucrados en la fractura así como el desplazamiento y rotación de la misma y la presencia de tejido muscular.

[Demo](https://youtu.be/GpmyILKOrR8)

## Prerrequisitos:

Descargar e instalar [Unity](https://unity3d.com/get-unity/download) (la versión en la que se desarrolló el proyecto es la versión 2018.3.0f2).

## Instalación y ejecución del proyecto:

Hay dos formas de ejecutar el proyecto:

### A. Ejecutar el proyecto en Unity:

- Descargar el proyecto.
- Asegurarse de que la carpeta "Casos" (ubicada en \Extension OrtHaptica\Casos) se encuentre creada, en caso contrario crearla.
- Abrir el proyecto en Unity.
- Ejecutar la escena inicial (ubicada en Assets/scenes/EscenaInicial).

### B. Ejecutar el proyecto como aplicación de escritorio:

- Descargar el proyecto.
- Abrir el proyecto en Unity.
- En el menú de Unity ir a la opción File -> Build settings
- Asegurarse que todas las escenas del proyecto se encuentren seleccionadas en el panel "scenes in build".
- Seleccionar la opción "PC, Mac & Linux Standalone" en el panel "Platform".
- Dar click en la opción "Build" para que se genere el build del proyecto.
- Una vez terminado el proceso ir a la carpeta generada por el build del proyecto y crear una nueva carpeta llamada Casos (en esta carpeta se guardarán los casos de fractura generados por la aplicación, por lo que si la carpeta no es creada dichos casos no se guardarán).
- Ejecutar la aplicación "Extension OrtHaptica" ubicada en la carpeta del build del proyecto generada en los pasos anteriores.


## Uso de la aplicación:

- Al ejecutar la aplicación se podrá escoger el hueso para el caso de fractura.
- Usar los controles mostrados en pantalla y el botón "Cortar Hueso" para realizar uno o más cortes en el hueso con la posición y rotación deseada. Para continuar a la siguiente etapa dar click en el botón "Mover Segmentos".
- Seleccionar cada segmento de hueso usando el mouse y modificar su posición y rotación a través de los controles mostrados en pantalla. Para continuar a la siguiente etapa dar click en el botón "Agregar Tejido".
- Seleccionar el tipo de tejido deseado.
- Determinar la posición y la rotación de la incisión que se realizará en el tejido utilizando los controles mostrados en pantalla. Para realizar la incisión en el tejido seleccionar la opción "Realizar Corte Tejido".
- Guardar el caso creado al dar click en la opción "Guardar" (se debe proporcionar la información solicitada al guardar el caso, es decir; nombre del caso, tipo de fractura y dificultad del caso).

Al guardar el caso de fractura se generarán dos archivos en formato JSON: <nombre_del_caso>.json y <nombre_del_caso>_Exportado.json.
Estos archivos quedarán guardados automáticamente en la carpeta Casos ubicada en \Extension OrtHaptica\Casos o en \<carpeta_build>\Casos dependiendo de la opción que se escogió para instalar y ejecutar el proyecto (ver la sección Instalación y ejecución del proyecto).

Los dos archivos mencionados previamente serán utilizados por OrtHáptica para el caso de entrenamiento.

## Integración con OrtHáptica

Para la integración con OrtHáptica se debe utilizar el proyecto [Generador Casos de Fractura](https://gitlab.com/babohorquez10/generador-casos-de-fractura), el cual toma como base los dos archivos JSON generados previamente para reconstruir el caso de fractura diseñado e integrarlo con OrtHáptica.


